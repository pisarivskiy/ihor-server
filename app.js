var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var sms = require('./libs/smsSender');
var moment = require('moment');
moment.locale('ru');

var settings = {
  sms: false
};
var messages = [];

var app = express();
var server = http.createServer(app);
app.locals.moment = moment;
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res, next) {
  res.render('index', { title: 'Умный почтовый ящик', messages: messages, settings: settings });
});
app.get('/count', function(req, res, next) {
  messages.push({
    text: 'Пришло новое письмо',
    date: Date.now(),
    sms: (settings.sms) ? 'SMS-оповещение' : 'Без SMS-оповещения'
  });
  if (settings.sms) {
    sms('380956317437', 'Novaya pochta: ' + messages.length, function (err) {
      if (err) {
        return res.send(err.message);
      }
      res.send('ok. sms is sent');
    });
  } else {
    res.send('ok');
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var io = require('socket.io')(server);
io.on('connection', function(socket){
  var timer = setInterval(function () {
    socket.emit('parcels', messages);
  } , 2000);
  socket.on('sms-trigger', function(data){
    settings.sms = data;
  });
  socket.on('clear', function(){
    messages = [];
  });
  socket.on('disconnect', function(){
    clearInterval(timer);
  });
});

server.listen(3000);
