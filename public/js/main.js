$(document).foundation();
moment.locale('ru');
var socket = io();
var parcels = $('#parcels');
var emptyLable = $('#empty');
var emptyLable = $('#empty');
var clearBtn = $('#clear');
var smsTrigger = $('#sms-trigger');

socket.on('parcels', function (data) {
  parcels.empty();
  var html = '';
  for (var i = 0; i<data.length;i++) {
    html += `<div class="parcel"><span class="number">${i + 1}</span><span class="text">${data[i].text}</span><span class="date">${moment(data[i].date).format('LLLL')}</span><span class="sms">${data[i].sms}</span></div>`;
  }
  parcels.append(html);
  if (data.length !== 0) {
    emptyLable.addClass('hide');
    clearBtn.removeClass('hide');
  } else {
    emptyLable.removeClass('hide');
    clearBtn.addClass('hide');
  }
});


smsTrigger.on('change', function () {
  socket.emit('sms-trigger', smsTrigger.is(':checked'));
});


clearBtn.on('click', function () {
  socket.emit('clear', null);
  parcels.empty();
  emptyLable.removeClass('hide');
  clearBtn.addClass('hide');
});